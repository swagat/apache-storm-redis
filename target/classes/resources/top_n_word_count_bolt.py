import storm
# Counter is a nice way to count things,
# but it is a Python 2.7 thing
from collections import Counter


class topNCountBolt(storm.BasicBolt):
    # Initialize this instance
    def initialize(self, conf, context):
        self._conf = conf
        self._context = context

        # Create a new counter for this instance
        self._counter = Counter()
        storm.logInfo("Top N Counter bolt instance starting...")

        # Hint: Add necessary instance variables and classes if needed
        self.previous_word = None
        self.previous_count = 0

    def process(self, tup):
        # TODO
        # Task: word count
        # Hint: using instance variable to tracking the word count
        word = tup.values[0]
        self._counter[word] += 1
        count = self._counter[word]
        #storm.logInfo("Top N Word Count %s:%s" % (word, count))
        # Emit the word and count
        #storm.emit([word, count])
        #----------------------
        if(word == 'eof'):
            for word, occurences in self._counter.iteritems():
                storm.logInfo("Top N Count Emitting: %s:%s" % (word, occurences))
                if (word != 'eof'):
                    storm.emit([word, occurences])
            storm.emit(['eof', self._counter['eof']])
        #----------------------
        pass
# Start the bolt when it's invoked
topNCountBolt().run()
