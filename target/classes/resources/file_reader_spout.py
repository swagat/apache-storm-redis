# import os
# from os.path import join
from time import sleep

# from streamparse import Spout
import storm
import time
import random
import datetime


class FileReaderSpout(storm.Spout):

    def initialize(self, conf, context):
        self._conf = conf
        self._context = context
        self._complete = False
        self.count = 0
        storm.logInfo("Spout instance starting...:%s" % datetime.datetime.now())

        # TODO:
        # Task: Initialize the file reader
        self._file = self._conf['topology.local.dir']
        storm.logInfo(self._file)
        self._file_reader = open(self._file, 'r')

        pass

    def nextTuple(self):
        #storm.logInfo("Spout instance nextTuple called...%s" % self.count)
        # TODO:
        # Task 1: read the next line and emit a tuple for it
        try:
            sentences = [line.strip() for line in self._file_reader.readlines() if line.strip()]
            for sentence in sentences:
                #storm.logInfo("SentenceEmiting:%s" % sentence)
                storm.emit([sentence])
            storm.emit(['EOF'])
            self._file_reader.close()
            pass
        except:
            pass
        # Task 2: don't forget to sleep for 1 second when the file is entirely read to prevent a busy-loop
        time.sleep(0.1)

        # End


# Start the spout when it's invoked
FileReaderSpout().run()
