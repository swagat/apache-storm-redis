**1 Overview and Requirements**
This assignment is going to build on Machine Problem 6: Flux Tutorial. It is highly recommended that you practice that tutorial before starting this assignment. We are going to use Storm V1.2.3, Redis 5.0.7, Python 2.7 and Open JDK 8. Note: the reason why we choose python2 instead of python3 in this MP is because Storm does not officially support python3 probably (See this github issue for more details: https://github.com/Azure-Samples/hdinsight-python-storm-wordcount/issues/3).

Note: for the storm-redis integration part, you still need to write two simple redis store bolt in java. See Redis Storm Integration section in storm tutorial for more details.

**2 Set up the environment**

Step 1: Start the "default" Docker machine that you created when following the "Tutorial: Docker installation" in week 4, run:

Install Storm 1.2.3

https://www.javahabit.com/2015/12/26/how-to-set-up-apache-storm-on-mac-using-brew/

Veevas-MacBook-Pro-8:~ sunilkulkarni$ brew search storm@1.2.3
No formula or cask found for "storm@1.2.3".
Closed pull requests:
storm 1.2.3 (https://github.com/Homebrew/homebrew-core/pull/43453)

Get RAW file from https://github.com/Homebrew/homebrew-core/pull/43453

Install Storm 1.2.3 
brew install https://raw.githubusercontent.com/chenrui333/homebrew-core/842d7a337e4f5a8109a9a2af79eea6d90e2c65f3/Formula/storm.rb

Update storm.yaml file with the following
storm.zookeeper.servers: 
    - "localhost"
    # – "server2"
# nimbus.host: "localhost" 
nimbus.thrift.port: 6627 
ui.port: 8772 
storm.local.dir: "/Users/chaklader/storm/data" 
java.library.path: /usr/lib/jvm
supervisor.slots.ports: 
    - 6700
    - 6701
    - 6702
    - 6703

Install Redis (brew install)

Veevas-MacBook-Pro-8:MP6_python_template sunilkulkarni$ pyenv versions
* system (set by /Users/sunilkulkarni/.pyenv/version)
  3.6.9
  3.6.9/envs/list
  3.6.9/envs/python36
  3.8.0
  3.8.0/envs/python38
  list
  python36
  python38
Veevas-MacBook-Pro-8:MP6_python_template sunilkulkarni$ 

#pyenv global python36 (to change to other python env) - This assignment requires python 2.7 (system)



Start: /usr/local/opt/storm/libexec/bin/storm nimbus
/usr/local/opt/storm/libexec/bin/storm supervisor
/usr/local/opt/storm/libexec/bin/storm ui


Veevas-MacBook-Pro-8:MP6_python_template sunilkulkarni$ pwd
/Users/sunilkulkarni/PycharmProjects/CS498/MP6_python_template

mvn clean compile package


storm jar ./target/WordCount-1.0-SNAPSHOT.jar org.apache.storm.flux.Flux -l -R /topology.yaml

redis-cli -h 127.0.0.1 -p 6379 -a uiuc_cs498_mp6 HGETALL partAWordCount
redis-cli -a uiuc_cs498_mp6 FLUSHALL


**Exercise A: Simple Word Count Topology**

In this exercise, you are going to build a simple word counter that counts the words a random sentence spout generates. This first exercise is similar to Machine Problem 6: Flux Tutorial.

In this exercise, we are going to use the random_sentence_spout.py as the spout, the split_sentence_bolt.py to split sentences into words, word_count_bolt.py class to count the words and RedisStoreBolt to save the output in Redis. The necessary knowledge has already been covered in the Tutorial.

For the splitter, we split the sentences at any non-alphanumerical characters (based on this regex syntax[^a-zA-Z0-9-]: it says we should split at "@", "!", "[", " "..., but not split at numbers or letters).

To save the output to redis, you should save field-value pairs ({word}, {count}) using hash value partAWordCount.

We've provided you the template in src/main/java/edu/illinois/storm/WordCountStoreMapper.java. To make it clear, in the auto-grader, we retrieve your answer from Redis by executing script equivalent to (Our auto-grader compiles and runs your submission in the same way):

`redis-cli -a uiuc_cs498_mp6 HGET partAWordCount apple`

You may find the hash name for the other parts in their template topology yaml files we provide.

You need to implement all components and to wire up these components. To make the implementation easier, we have provided template. Keep in mind that the most important task for this part is to master how to set up a storm application.

Note that, when auto-graded, this topology will run for 30 seconds and automatically gets killed after that (You should guarantee your code be completed within this time limit. Otherwise, you cannot pass.).

You can build and run the application using the command below inside the container:

`cd <MP6_folder>
mvn clean package
storm jar ./target/storm-example-0.0.1-SNAPSHOT.jar org.apache.storm.flux.Flux --local -R /part_a_topology.yaml -s 30000`

If your solution is right, you should see the corresponding result in Redis. We suggest you think about how you can debug your solution efficiently.

**Exercise B: Input Data from a File**
As can be seen, the spout used in the topology of Exercise A is generating random sentences from a predefined set in the spout’s class. However, we want to count words for a given corpus. Thus, in this exercise, you are going to create a new spout that reads data from an input file and emits each line as a tuple. Remember to put a 1-second sleep after reading the whole file to avoid a busy loop.

To make the implementation easier, we have provided a template for the spout needed in the following file: multilang/resources/file_reader_spout.py.

After finishing the implementation of file_reader_spout.py, you have to wire up the topology with this new spout.

To make the implementation easier, we have provided a template for the topology needed in the following file: src/main/resources/part_b_topology.yaml. To help with developing and debugging your solution, you can use the following dataset: “https://raw.githubusercontent.com/UIUC-CS498-Cloud/Docker_MP6/master/data.txt”. Please download and move it to “/tmp/data.txt” path in your docker.

To save the output to redis, you should save field-value pairs ({word}, {count}) using hash value partBWordCount. We've provided you the template in src/main/java/edu/illinois/storm/WordCountStoreMapper.java. To make it clear, in the auto-grader, we retrieve your answer from Redis by executing script equivalent to:

`redis-cli -a uiuc_cs498_mp6 HGET partBWordCount apple`

You need to implement all components and to wire up these components. To make the implementation easier, we have provided templates.

Note that, when auto-graded, this topology will run for 30 seconds and automatically gets killed after that (You should guarantee your code be completed within this time limit. Otherwise, you cannot pass.).

NOTE: You probably want to set the number of executors of the spout to “1” so that you don’t read the input file more than once. However, you have the freedom to have a different implementation as long as the result is correct.

You can build and run the application using the command below inside the container:

`cd <MP6_folder>
mvn clean package
storm jar ./target/storm-example-0.0.1-SNAPSHOT.jar org.apache.storm.flux.Flux --local -R /part_b_topology.yaml -s 30000`

Note that you'll need to set the path of the input file in part_b_topology.yaml. We've covered how to pass configuration to spout. You can think about how you can set the input file path in part_b_topology.yaml. We will put the input data at /tmp/data.txt in the auto-grader. You should modify your path accordingly before pack your solution.

If your solution is right, you should see the corresponding result in Redis:

`redis-cli -a uiuc_cs498_mp6 HGET partBWordCount no # result: ”19”
redis-cli -a uiuc_cs498_mp6 HGET partBWordCount it # result: “7”
redis-cli -a uiuc_cs498_mp6 HGET partBWordCount the # result: “6”`

**Exercise C: Normalizer Bolt**

The application we developed in Exercise B counts the words “Apple” and “apple” as two different words. However, if we want to find the top N words, we have to count these words the same. Additionally, we don’t want to take common English words into consideration.

Therefore, in this part, we are going to normalize the words by adding a normalizer bolt that gets the words from the splitter, normalizes them, and then sends them to the counter bolt. The responsibility of the normalizer is to:

Make all input words lowercase.
Remove common English words.
To make the implementation easier, we have provided a template for the normalizer bolt in the following file: multilang/resources/normalizer_bolt.py. To help with developing and debugging your solution, you can use the following dataset: “https://raw.githubusercontent.com/UIUC-CS498-Cloud/Docker_MP6/master/data.txt”. Please download and move it to “/tmp/data.txt” path in your docker.

There is a list of common words to filter in this class, so please make sure you use this exact list to in order to receive the maximum points for this part. After finishing the implementation of this class, you have to wire up the topology with this bolt added to the topology.

To save the output to redis, you should save field-value pairs ({word}, {count}) using hash value partCWordCount. We've provided you the template in src/main/java/edu/illinois/storm/WordCountStoreMapper.java. To make it clear, in the auto-grader, we retrieve your answer from Redis by executing script equivalent to:

`redis-cli -a uiuc_cs498_mp6 HGET partCWordCount apple`

You need to implement all components and to wire up these components. To make the implementation easier, we have provided templates.

Note that, when auto-graded, this topology will run for 30 seconds and automatically gets killed after that (You should guarantee your code be completed within this time limit. Otherwise, you cannot pass.).

You can build and run the application using the command below inside the container:

`cd <MP6_folder>
mvn clean package
storm jar ./target/storm-example-0.0.1-SNAPSHOT.jar org.apache.storm.flux.Flux --local -R /part_c_topology.yaml -s 30000`

If your solution is right, you should see the corresponding result in Redis:

`redis-cli -a uiuc_cs498_mp6 HGET partCWordCount cause # result: ”2”
redis-cli -a uiuc_cs498_mp6 HGET partCWordCount yet # result: “16”
redis-cli -a uiuc_cs498_mp6 HGET partCWordCount had # result: “2”`

**Exercise D: Top N Words**


In this exercise, we are going to add a new bolt which uses the output of the count bolt to keep track of and report the top N words. Upon receipt of a new count from the count bolt, it updates the top N words and emits top N words set anytime it changes.

To output the top N words set, you should use ", " (don't miss the space) connect all top words. You don't need to worry about the sequence. The result should contain and only contain the top N words. For example, if 3-top words are "blue", "red" and "green", "blue, red, green", "red, blue, green" are all correct answer.

To save the output to Redis, you should save field-value pairs ("top-N", {top N words string}) using hash value partDTopN. It's not the best way to save a set in Redis, but Redis is not the key point for this assignment. So we decided to make it easier for you to implement by keeping using hashes in part d. We've provided you the template in src/main/java/edu/illinois/storm/TopNStoreMapper.java. To help with developing and debugging your solution, you can use the following dataset: “https://raw.githubusercontent.com/UIUC-CS498-Cloud/Docker_MP6/master/data.txt”. Please download and move it to “/tmp/data.txt” path in your docker. To make it clear, in the auto-grader, we retrieve your answer from Redis by executing script equivalent to:


`redis-cli -a uiuc_cs498_mp6 HGET partDTopN top-N
# the output for example above should be:
# "blue, red, green"`

After finishing the implementation of this class, you have to wire up the topology with this bolt added to the topology.

You need to implement all components and to wire up these components. To make the implementation easier, we have provided templates.

Note that, when auto-graded, this topology will run for 30 seconds and automatically gets killed after that (You should guarantee your code be completed within this time limit. Otherwise, you cannot pass.). For this part, different algorithm will have huge difference in performance. We suggest you to refine your algorithm to make it more efficient if you failed in this part while you get correct answer locally. Algorithm with time complexity of O(nN) seems to be save to pass where n is the number of total words and N is the amount of top words you need track.

You can build and run the application using the command below inside the container:

`cd <MP6_folder>
mvn clean package
storm jar ./target/storm-example-0.0.1-SNAPSHOT.jar org.apache.storm.flux.Flux --local -R /part_d_topology.yaml -s 30000`

If your solution is right, you should see the corresponding result in Redis:

`redis-cli -a uiuc_cs498_mp6 HGET partDTopN top-N # result: "far, oh, yet, up, get, ye, no, sir, mr, day" (the order does not matter here)`

